# VueJS do Jeito Ninja

- https://www.youtube.com/watch?v=07-TvnH7XNo&list=PLcoYAcR89n-qq1vGRbaUiV6Q9puy0qigW

## Intância do VueJS
```javascript
var app = new Vue({
    el: '#app', // id da div que vai ficar a aplicação
    data: {
        // Código vue ...
        texto: false,
        titulo: "Aula 01 - Vuejs do Jeito Ninja"
    }
});
```        


## Diretivas

- v-if/v-else => apaga o elemento do HTML (caso usado em um elemento). Ele faz com que aquilo não exista caso usar
essa condição
- v-show => apenas aplica um display none no elemento
- v-for => Faz um loop padrão: ```<li v-for="item in array">``` ou se quiser o index ```<li v-for="(item, index) in array">```
- v-model => atribui valor a alguma variável, geralmente usada no formulário. Faz o data binding
- v-bind => faz o binding com algum atributo do HTML: ```<img v-bind:src="variavel">``` ou ```<img :src="variavel">```




- v-on:event ou @event